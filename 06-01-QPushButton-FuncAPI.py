
# 0. 导入需要的包和模块
from PyQt5.Qt import *  # 主要包含了常用的一些类
from PyQt5 import QtCore, QtGui
import sys

class Window(QWidget):
    def contextMenuEvent(self, a0: QtGui.QContextMenuEvent) -> None:
        print('展示菜单')
        # 子菜单 最近打开
        # 行为动作 新建 打开 分割线 退出
        btnMenu = QMenu(self)

        btnMenu.addMenu('最近打开')
        btnMenu.addAction('新建')
        btnMenu.addAction('打开')
        btnMenu.addSeparator()
        btnMenu.addAction('退出')
        btnSubMenu1 = btnMenu.addMenu('menu1')
        btnSubMenu1.addAction('subMeau1_Act1')
        btn.setMenu(btnMenu)

        btnMenu.exec_(a0.globalPos())
        # btnMenu.exec_(a0.pos())


# 1. 创建一个应用程序对象
# sys.argv是从用户处接收到的命令参数 这里传递给Qt的程序
app = QApplication(sys.argv)

# 2. 控件的操作
# 2.1 创建控件
# window = QWidget()
window = Window()


# 2.2 设置控件
window.setWindowTitle("QPushButton功能API测试")
window.resize(500, 500)

btn = QPushButton(QIcon('xxx.png'), 'Button', window)

# *************** 菜单 *************** 开始
# 子菜单 最近打开
# 行为动作 新建 打开 分割线 退出
btnMenu = QMenu(btn)

# actionNew = QAction(,'新建')
btnMenu.addMenu('最近打开')
btnMenu.addAction('新建')
btnMenu.addAction('打开')
btnMenu.addSeparator()
btnMenu.addAction('退出')
btnSubMenu1 = btnMenu.addMenu('menu1')
btnSubMenu1.addAction('subMeau1_Act1')
btn.setMenu(btnMenu)

# *************** 子菜单 *************** 开始
# btn.showMenu()
# btn = QPushButton()
# btn.setVisible(True)
# btn.setParent(window)
# btn.setText('Button')
# btn.setIcon(QIcon('xxx.png'))


# *************** 默认按钮 *************** 开始
btn2 = QPushButton('默认按钮', window)
btn2.move(300, 300)
btn2.setAutoDefault(True)

print(btn.autoDefault())
print(btn2.autoDefault())

btn2.setDefault(True)
# *************** 默认按钮 *************** 结束

# 设置为默认上下文菜单
# window.setContextMenuPolicy(Qt.DefaultContextMenu)
# 设置为自定义上下文菜单 需接收发射的信号customContextMenuRequested
def customMenu(point):
    print('自定义上下文菜单', point)
    # 子菜单 最近打开
    # 行为动作 新建 打开 分割线 退出
    btnMenu = QMenu(window)

    btnMenu.addMenu('最近打开')
    btnMenu.addAction('新建')
    btnMenu.addAction('打开')
    btnMenu.addSeparator()
    btnMenu.addAction('退出')
    btnSubMenu1 = btnMenu.addMenu('menu1')
    btnSubMenu1.addAction('subMeau1_Act1')
    btn.setMenu(btnMenu)
    # 此处需要为全局坐标 customContextMenuRequested 传递的为对应父控件的坐标
    # 使用全局坐标映射方法可得全局坐标
    dstPoint = window.mapToGlobal(point)
    btnMenu.exec_(dstPoint)
window.setContextMenuPolicy(Qt.CustomContextMenu)
# customContextMenuRequested
window.customContextMenuRequested.connect(customMenu)
# 2.3 展示控件
window.show()

# 3. 应用程序的执行，进入到消息循环
# 让整个程序开始执行， 并且进入到消息循环（无线循环）
# 检测整个程序所接收到的用户的交互信息

sys.exit(app.exec_())