#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Created on 2021/12/28 0028:12:49
@author: muyuyang123
@site: https://gitee.com/muyuyang123_admin
@email: 13838164635@163.com
@file: 
@description: 
"""
from PyQt5.Qt import *  # 主要包含了我们常用的一些类, 汇总到了一块
import sys

# 1. 创建一个应用程序对象
app = QApplication(sys.argv)

# 2. 控件的操作
# 2.1 创建控件
window = QWidget()

# 2.2 设置控件
window.setWindowTitle("的学习")
window.resize(500, 500)


# 2.3 展示控件
window.show()

# 3. 应用程序的执行, 进入到消息循环
sys.exit(app.exec_())
