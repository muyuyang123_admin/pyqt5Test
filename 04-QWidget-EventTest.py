#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Created on 2022/1/22
@author: muyuyang123
@site: https://gitee.com/muyuyang123_admin
@email: 13838164635@163.com
@file: 
@description: 
"""
# 0. 导入需要的包和模块
from PyQt5.Qt import *


class Window(QWidget):
    def __init__(self):
        super(Window, self).__init__()
        self.setWindowTitle("QWidget事件消息")
        self.resize(500, 500)
        self.setup_ui()

    def setup_ui(self):
        pass

    def showEvent(self, QShowEvent):
        print('窗口被展示了')

    def closeEvent(self, QCloseEvent):
        print('窗口被关闭了')

    def moveEvent(self, QMoveEvent):
        print('窗口被移动了')

    def resizeEvent(self, a0: QResizeEvent) -> None:
        print('窗口被改变大小了')

    def enterEvent(self, a0: QEvent) -> None:
        print('鼠标进来了')
        self.setStyleSheet('background: green;')

    def leaveEvent(self, a0: QEvent) -> None:
        print('鼠标出来了')
        self.setStyleSheet('background: red;')

    def mousePressEvent(self, a0: QMouseEvent) -> None:
        print('鼠标被按下')

    def mouseReleaseEvent(self, a0: QMouseEvent) -> None:
        print('鼠标被释放')

    def mouseDoubleClickEvent(self, a0: QMouseEvent) -> None:
        print('鼠标被双击')

    def mouseMoveEvent(self, a0: QMouseEvent) -> None:
        print('鼠标移动了')

    def keyPressEvent(self, a0: QKeyEvent) -> None:
        print('键盘上某一个按键被按下了')

    def keyReleaseEvent(self, a0: QKeyEvent) -> None:
        print('键盘上某一个按键被松开了')


if __name__ == '__main__':
    import sys

    # 1. 创建一个应用程序对象
    app = QApplication(sys.argv)

    # 2. 控件的操作
    # 创建控件,设置控件(大小,位置,样式...),事件,信号的处理
    # 2.1 创建控件
    window = Window()

    # 2.2 设置控件
    # window.setWindowTitle("pyqt学习")
    # window.resize(500, 500)

    # 2.3 展示控件
    window.show()

    # 3. 应用程序的执行, 进入到消息循环
    # 检测整个程序所接收到的用户的交互信息
    sys.exit(app.exec_())

