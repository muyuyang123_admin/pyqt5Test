
# 0. 导入需要的包和模块
from PyQt5.Qt import *  # 主要包含了常用的一些类
import sys
from PyQt5 import QtGui

class MyLabel(QLabel):
    def mousePressEvent(self, ev: QtGui.QMouseEvent) -> None:
        self.raise_()

class MyWidget(QWidget):
    def mousePressEvent(self, a0: QtGui.QMouseEvent) -> None:
        childWidget = self.childAt(a0.x(), a0.y())
        if childWidget:
            childWidget.raise_()
# 1. 创建一个应用程序对象
# sys.argv是从用户处接收到的命令参数 这里传递给Qt的程序
app = QApplication(sys.argv)

# 2. 控件的操作
# 2.1 创建控件
# window = QWidget()
window = MyWidget()


# 2.2 设置控件
window.setWindowTitle("QWidget层级关系调整")
window.resize(500, 500)


label1 = QLabel(window)
# label1 = MyLabel(window)
label1.setText('label1')
label1.move(10, 10)
label1.setStyleSheet('background: red;')

label2 = QLabel(window)
# label2 = MyLabel(window)
label2.setText('label2')
label2.move(30, 15)
label2.setStyleSheet('background: green;')

# label1.raise_()
# label2.lower()
# label2.stackUnder(label1)

# 2.3 展示控件
window.show()

# 3. 应用程序的执行，进入到消息循环
# 让整个程序开始执行， 并且进入到消息循环（无线循环）
# 检测整个程序所接收到的用户的交互信息

sys.exit(app.exec_())