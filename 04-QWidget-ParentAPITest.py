#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Created on 2022/1/23
@author: muyuyang123
@site: https://gitee.com/muyuyang123_admin
@email: 13838164635@163.com
@file: 
@description: 
"""
# 0. 导入需要的包和模块
from PyQt5.Qt import *  # 主要包含了我们常用的一些类, 汇总到了一块
import sys


# 1. 创建一个应用程序对象
app = QApplication(sys.argv)

# 2. 控件的操作
# 创建控件,设置控件(大小,位置,样式...),事件,信号的处理
# 2.1 创建控件
window = QWidget()

# 2.2 设置控件
window.setWindowTitle("父子关系学习")
window.resize(500, 500)

label1 = QLabel(window)
# label1.setParent(window)
label1.setText('label1')

label2 = QLabel(window)
# label1.setParent(window)
label2.setText('label2')
label2.move(50, 50)

label3 = QLabel(window)
# label1.setParent(window)
label3.setText('label3')
label3.move(100, 100)

print(window.childAt(255, 255))

print(label2.parentWidget())

print(window.childrenRect())

# 2.3 展示控件
window.show()

# 3. 应用程序的执行, 进入到消息循环
# 检测整个程序所接收到的用户的交互信息
sys.exit(app.exec_())
