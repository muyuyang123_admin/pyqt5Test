# 0. 导入需要的包和模块
from PyQt5.Qt import *  # 主要包含了常用的一些类


class Window(QWidget):
    def __init__(self):
        # 调用父类的init方法
        super().__init__()
        self.setWindowTitle("交互状态的案例")
        self.resize(500, 500)
        self.setup_ui()

    def setup_ui(self):
        # 添加3个子控件
        self.label = QLabel(self)
        self.label.setText('登录成功')
        self.label.move(100, 50)
        self.label.hide()

        self.lineEdit = QLineEdit(self)
        self.lineEdit.move(100, 150)
        self.lineEdit.setText('')

        self.btn = QPushButton(self)
        self.btn.setText('登录')
        self.btn.move(100, 250)
        self.btn.setEnabled(False)

        # 构建逻辑
        self.lineEdit.textChanged.connect(self.change)
        self.btn.clicked.connect(self.login)
        pass

    def change(self):
        if len(self.lineEdit.text())>0:
            self.btn.setEnabled(True)
        else:
            self.btn.setEnabled(False)
            self.label.hide()

        # print(self.lineEdit.text())

    def login(self):
        if self.lineEdit.text() == 'Sz':
            self.label.setText('登录成功')
        else:
            self.label.setText('登录失败')
        self.label.show()
        self.label.adjustSize()




if __name__ == '__main__':
    import sys
    app = QApplication(sys.argv)

    window = Window()
    window.show()
    sys.exit(app.exec_())
