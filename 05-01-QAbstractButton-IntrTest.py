
# 0. 导入需要的包和模块
from PyQt5.Qt import *  # 主要包含了常用的一些类
import sys
from PyQt5 import QtGui


# 1. 创建一个应用程序对象
# sys.argv是从用户处接收到的命令参数 这里传递给Qt的程序
app = QApplication(sys.argv)

# 2. 控件的操作
# 2.1 创建控件
window = QWidget()


# 2.2 设置控件
window.setWindowTitle("QAbstractButton简单介绍")
window.resize(500, 500)

class Btn(QAbstractButton):
    def paintEvent(self, e: QtGui.QPaintEvent) -> None:
        # print('绘制按钮')
        # 1 创建一个画家 并设定绘制在什么地方
        painter = QPainter(self)

        # 2 给画家一个画笔
        # 2.1 创建画笔
        pen = QPen(QColor(0, 0, 0), 10)
        # 2.2 设置画笔
        painter.setPen(pen)

        # 画家画
        painter.drawText(20, 50, self.text())
        painter.drawEllipse(0, 0, 100, 100)

        # return super(Btn, self).paintEvent(e)
    pass

btn = Btn(window)
btn.setText('xxx')
btn.resize(100, 100)
btn.clicked.connect(lambda: print('点击按钮'))


# 2.3 展示控件
window.show()

# 3. 应用程序的执行，进入到消息循环
# 让整个程序开始执行， 并且进入到消息循环（无线循环）
# 检测整个程序所接收到的用户的交互信息

sys.exit(app.exec_())