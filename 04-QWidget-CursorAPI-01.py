#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Created on 2022/1/18
@author: muyuyang123
@site: https://gitee.com/muyuyang123_admin
@email: 13838164635@163.com
@file: 
@description: 
"""

# 0. 导入需要的包和模块
from PyQt5.Qt import *  # 主要包含了我们常用的一些类, 汇总到了一块
import sys

class MyQWidget(QWidget):
    # QtGui.QMouseEvent
    def mouseMoveEvent(self, a0):
        QMouseEvent
        print('mouse moving ', a0.localPos())


# 1. 创建一个应用程序对象
app = QApplication(sys.argv)

# 2. 控件的操作
# 创建控件,设置控件(大小,位置,样式...),事件,信号的处理
# 2.1 创建控件
window = MyQWidget()

# 2.2 设置控件
window.setWindowTitle("QWidget鼠标API")
window.resize(500, 500)
window.setMouseTracking(True)
# print(window.hasMouseTracking())

# window.setCursor(Qt.BusyCursor)
# window.setCursor(Qt.ForbiddenCursor)
# pixmap = QPixmap('xxx.png')
# pixmap = pixmap.scaled(50, 50)
# cursor = QCursor(pixmap)
# cursor = QCursor(pixmap, 50, 50)

# window.setCursor(cursor)
# window.unsetCursor()
# currentCursor = window.cursor()
# print(currentCursor.pos())
# window.cursor().setPos(0, 0)

# 2.3 展示控件
window.show()

# 3. 应用程序的执行, 进入到消息循环
# 检测整个程序所接收到的用户的交互信息
sys.exit(app.exec_())
