
# 0. 导入需要的包和模块
from PyQt5.Qt import *  # 主要包含了常用的一些类
import sys


# 1. 创建一个应用程序对象
# sys.argv是从用户处接收到的命令参数 这里传递给Qt的程序
app = QApplication(sys.argv)

# 2. 控件的操作
# 2.1 创建控件
window = QWidget()


# 2.2 设置控件
window.setWindowTitle("编辑状态[*]")
window.resize(500, 500)

# window.setWindowModified(True)

# w2 = QWidget()
# w2.setWindowTitle('w2')

btn = QPushButton(window)
btn.setText('button')
btn.destroyed.connect(lambda: print('按钮被销毁了'))

# btn.setVisible(False)
# btn.setHidden(True)
# btn.hide()
# btn.deleteLater()
btn.setAttribute(Qt.WA_DeleteOnClose)
btn.close()

# 2.3 展示控件
window.show()


# w2.show()

# window.raise_()
# print(w2.isActiveWindow())
# 3. 应用程序的执行，进入到消息循环
# 让整个程序开始执行， 并且进入到消息循环（无线循环）
# 检测整个程序所接收到的用户的交互信息

sys.exit(app.exec_())