
# 0. 导入需要的包和模块
from PyQt5.Qt import *  # 主要包含了常用的一些类
import sys


# 1. 创建一个应用程序对象
# sys.argv是从用户处接收到的命令参数 这里传递给Qt的程序
app = QApplication(sys.argv)

# 2. 控件的操作
# 2.1 创建控件
# window = QWidget()
window = QMainWindow()
# 懒加载
# 只有用到的时候才会创建
window.statusBar()

window.setWindowTitle("信息提示案例")
window.resize(500, 500)
window.setWindowFlags(Qt.WindowContextHelpButtonHint)

# 鼠标停留在设置的控件上后 在状态栏中显示的文字
window.setStatusTip('这是窗口')

label = QLabel(window)
label.resize(100, 100)
label.setText('标签')
label.setStatusTip('这是标签')
label.setToolTip('提示标签')
label.setToolTipDuration(1000)
print(label.toolTipDuration())

label.setWhatsThis('What?')
# print(window.statusTip())





# 2.3 展示控件
window.show()

# 3. 应用程序的执行，进入到消息循环
# 让整个程序开始执行， 并且进入到消息循环（无线循环）
# 检测整个程序所接收到的用户的交互信息

sys.exit(app.exec_())