#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Created on 2022/1/22
@author: muyuyang123
@site: https://gitee.com/muyuyang123_admin
@email: 13838164635@163.com
@file: 
@description: 
"""
# 0. 导入需要的包和模块
from PyQt5.Qt import *
from PyQt5 import QtGui


class Window(QWidget):
    def __init__(self):
        super(Window, self).__init__()
        self.setWindowTitle("窗口移动的学习")
        self.resize(500, 500)
        self.setup_ui()
        self.mousePressStatus = 0
        self.cursorPosX = self.pos().x()
        self.cursorPosY = self.pos().y()


    def setup_ui(self):

        pass

    def mousePressEvent(self, a0: QtGui.QMouseEvent) -> None:
        if a0.button() == Qt.LeftButton:
            self.mousePressStatus = 1
            # self.cursorPosX = a0.x()
            # self.cursorPosY = a0.y()
            self.curPos = self.pos()
            self.cursorPosX = a0.globalX()
            self.cursorPosY = a0.globalY()

            print('鼠标按下')

    def mouseReleaseEvent(self, a0: QtGui.QMouseEvent) -> None:
        self.mousePressStatus = 0
        print('鼠标释放')

    def mouseMoveEvent(self, a0: QtGui.QMouseEvent) -> None:
        print('鼠标移动')
        if self.mousePressStatus == 1:
            # curPos = self.pos()
            print('self pos: ', self.curPos.x(), self.curPos.y())
            # print('corsor pos :', a0.x(), a0.y())
            # self.move(curPos.x()+a0.x()-self.cursorPosX, curPos.y()+a0.y()-self.cursorPosY)
            print('corsor pos :', a0.globalX(), a0.globalY())
            self.move(self.curPos.x()+a0.globalX()-self.cursorPosX, self.curPos.y()+a0.globalY()-self.cursorPosY)

if __name__ == '__main__':
    import sys

    # 1. 创建一个应用程序对象
    app = QApplication(sys.argv)

    # 2. 控件的操作
    # 创建控件,设置控件(大小,位置,样式...),事件,信号的处理
    # 2.1 创建控件
    window = Window()

    # 2.2 设置控件
    # window.setWindowTitle("pyqt学习")
    # window.resize(500, 500)

    # 2.3 展示控件
    window.show()

    # 3. 应用程序的执行, 进入到消息循环
    # 检测整个程序所接收到的用户的交互信息
    sys.exit(app.exec_())