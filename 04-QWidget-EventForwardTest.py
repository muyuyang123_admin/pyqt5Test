#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Created on 2022/1/22
@author: muyuyang123
@site: https://gitee.com/muyuyang123_admin
@email: 13838164635@163.com
@file: 
@description: 
"""
# 0. 导入需要的包和模块
from PyQt5.Qt import *
from PyQt5 import QtGui


class Window(QWidget):
    pass

    def mousePressEvent(self, a0: QtGui.QMouseEvent) -> None:
        print('顶层窗口鼠标按下')

class MidWindow(QWidget):
    pass
    #
    # def mousePressEvent(self, a0: QtGui.QMouseEvent) -> None:
    #     print('中间控件鼠标按下')

class MyLabel(QLabel):
    # pass

    def mousePressEvent(self, ev: QtGui.QMouseEvent) -> None:
        # print('标签控件鼠标按下')
        # ev.accept()
        # print(ev.isAccepted())
        ev.ignore()
        print(ev.isAccepted())


if __name__ == '__main__':
    import sys

    # 1. 创建一个应用程序对象
    app = QApplication(sys.argv)

    # 2. 控件的操作
    # 创建控件,设置控件(大小,位置,样式...),事件,信号的处理
    # 2.1 创建控件
    window = Window()
    midWindow = MidWindow(window)
    label = MyLabel(midWindow)

    # 2.2 设置控件
    window.setWindowTitle("事件转发")
    window.resize(500, 500)
    midWindow.resize(300, 300)
    midWindow.move(100, 100)
    midWindow.setStyleSheet('background-color: green;')
    midWindow.setAttribute(Qt.WA_StyledBackground, True)

    label.setText('label')
    label.resize(100, 100)
    label.move(100, 100)
    label.setStyleSheet('background-color: red;')

    # 2.3 展示控件
    window.show()
    # midWindow.show()
    # label.show()

    # 3. 应用程序的执行, 进入到消息循环
    # 检测整个程序所接收到的用户的交互信息
    sys.exit(app.exec_())