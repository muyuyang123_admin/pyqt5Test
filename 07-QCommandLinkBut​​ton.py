
# 0. 导入需要的包和模块
from PyQt5.Qt import *  # 主要包含了常用的一些类
import sys


# 1. 创建一个应用程序对象
# sys.argv是从用户处接收到的命令参数 这里传递给Qt的程序
app = QApplication(sys.argv)

# 2. 控件的操作
# 2.1 创建控件
window = QWidget()


# 2.2 设置控件
window.setWindowTitle("QCommandLinkButtton学习")
window.resize(500, 500)

commandBtn = QCommandLinkButton('命令链接按钮', '描述', window)
commandBtn.move(100, 100)
commandBtn.setIcon(QIcon('xxx.png'))

print(commandBtn.description())



# 2.3 展示控件
window.show()

# 3. 应用程序的执行，进入到消息循环
# 让整个程序开始执行， 并且进入到消息循环（无线循环）
# 检测整个程序所接收到的用户的交互信息

sys.exit(app.exec_())