
# 0. 导入需要的包和模块
from PyQt5.Qt import *  # 主要包含了常用的一些类
import sys
from PyQt5 import QtGui, QtCore

class Window(QWidget):
    def __init__(self):
        super(Window, self).__init__()
        # self.setWindowTitle("")
        self.resize(500, 500)
        self.mousePressStatus = 0
        self.cursorPosX = self.pos().x()
        self.cursorPosY = self.pos().y()
        # self.setWindowTitle("顶层窗口操作案例")
        self.setWindowOpacity(0.9)
        self.setWindowFlags(Qt.CustomizeWindowHint)
        self.setWindowFlags(Qt.FramelessWindowHint)
        self.btnTopMargin = 10
        self.btnWidth = 100
        self.btnHeigth = 60

        self.setup_ui()

    def setup_ui(self):
        self.closeBtn = QPushButton(self)
        self.closeBtn.setText('关闭')
        self.closeBtn.resize(self.btnWidth, self.btnHeigth)
        # closeBtnW = self.closeBtn.width()
        # windowWidth = self.width()
        # self.closeBtn.move(windowWidth-closeBtnW, self.btnTopMargin)
        self.closeBtn.clicked.connect(self.close)

        self.maxBtn = QPushButton(self)
        self.maxBtn.setText('最大化')
        self.maxBtn.resize(self.btnWidth, self.btnHeigth)
        # maxBtnW = self.maxBtn.width()
        # self.maxBtn.move(windowWidth-closeBtnW-maxBtnW, self.btnTopMargin)
        self.maxBtn.clicked.connect(self.maxNormal)

        self.minBtn = QPushButton(self)
        self.minBtn.setText('最小化')
        self.minBtn.resize(self.btnWidth, self.btnHeigth)
        # minBtnW = self.minBtn.width()
        # self.minBtn.move(windowWidth-closeBtnW-minBtnW-maxBtnW, self.btnTopMargin)
        self.minBtn.clicked.connect(self.showMinimized)
        # minBtnIcon = QIcon('xxx.png')
        # self.minBtn.setIcon(minBtnIcon)

        pass

    def maxNormal(self):
        if self.isMaximized():
            self.showNormal()
            self.maxBtn.setText('最大化')
            # closeBtnW = self.closeBtn.width()
            # windowWidth = self.width()
            # self.closeBtn.move(windowWidth - closeBtnW, self.btnTopMargin)
            # maxBtnW = self.maxBtn.width()
            # self.maxBtn.move(windowWidth - closeBtnW - maxBtnW, self.btnTopMargin)
            # minBtnW = self.minBtn.width()
            # self.minBtn.move(windowWidth - closeBtnW - minBtnW - maxBtnW, self.btnTopMargin)
        else:
            self.showMaximized()
            self.maxBtn.setText('还原')


    def resizeEvent(self, a0: QtGui.QResizeEvent) -> None:
        closeBtnW = self.closeBtn.width()
        windowWidth = self.width()
        self.closeBtn.move(windowWidth - closeBtnW, self.btnTopMargin)
        maxBtnW = self.maxBtn.width()
        self.maxBtn.move(windowWidth - closeBtnW - maxBtnW, self.btnTopMargin)
        minBtnW = self.minBtn.width()
        self.minBtn.move(windowWidth - closeBtnW - minBtnW - maxBtnW, self.btnTopMargin)

    def mousePressEvent(self, a0: QtGui.QMouseEvent) -> None:
        if a0.button() == Qt.LeftButton:
            self.mousePressStatus = 1
            # self.cursorPosX = a0.x()
            # self.cursorPosY = a0.y()
            self.curPos = self.pos()
            self.cursorPosX = a0.globalX()
            self.cursorPosY = a0.globalY()

            # print('鼠标按下')

    def mouseReleaseEvent(self, a0: QtGui.QMouseEvent) -> None:
        self.mousePressStatus = 0
        # print('鼠标释放')

    def mouseMoveEvent(self, a0: QtGui.QMouseEvent) -> None:
        # print('鼠标移动')
        if self.mousePressStatus == 1:
            # curPos = self.pos()
            # print('self pos: ', self.curPos.x(), self.curPos.y())
            # print('corsor pos :', a0.x(), a0.y())
            # self.move(curPos.x()+a0.x()-self.cursorPosX, curPos.y()+a0.y()-self.cursorPosY)
            # print('corsor pos :', a0.globalX(), a0.globalY())
            self.move(self.curPos.x()+a0.globalX()-self.cursorPosX, self.curPos.y()+a0.globalY()-self.cursorPosY)


# 1. 创建一个应用程序对象
# sys.argv是从用户处接收到的命令参数 这里传递给Qt的程序
app = QApplication(sys.argv)

# 2. 控件的操作
# 2.1 创建控件
window = Window()
# window = QWidget()
# window = QWidget(flags=Qt.FramelessWindowHint)


# 2.2 设置控件



# 2.3 展示控件
window.show()

# 3. 应用程序的执行，进入到消息循环
# 让整个程序开始执行， 并且进入到消息循环（无线循环）
# 检测整个程序所接收到的用户的交互信息

sys.exit(app.exec_())