#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Created on 2022/1/4 0004:12:46
@author: muyuyang123
@site: https://gitee.com/muyuyang123_admin
@email: 13838164635@163.com
@file: 
@description: 
"""
from PyQt5.Qt import *

class Window(QWidget):
    def __init__(self):
        super().__init__()
        self.setWindowTitle("QObeject的学习")
        self.resize(500, 500)
        self.setup_ui()

    def setup_ui(self):
        # self.QObejectInheritTest()
        # self.QObejectAPITest()
        # self.QObejectSignalTest()
        # self.QObjectTypeTest()
        self.QObjectDelObjTest()
        pass

    def QObejectInheritTest(self):
        mros = QObject.mro()
        for mro in mros:
            print(mro)


    def QObejectAPITest(self):
        # 测试API
        obj = QObject()
        obj.setObjectName('notice')
        print(obj.objectName())


    def QObejectSignalTest(self):
        # ********** 信号与槽测试案例 **********
        #
        # btn = QPushButton(self)
        # btn.setText('点击我')
        #
        # def cao():
        #     print('点击我嘎哈')
        #
        # btn.clicked.connect(cao)
        # ********** 信号与槽测试案例 **********
        pass

    # QObeject类型判定
    def QObjectTypeTest(self):

        # ********** API ********** 开始
        # obj = QObject()
        # w = QWidget()
        # btn = QPushButton()
        # label = QLabel()
        # 
        # objs = [obj, w, btn, label]
        # for o in objs:
        #     # 是否为控件类型
        #     # print(o.isWidgetType())
        #     # 判断直接或间接父类
        #     # print(o.inherits('QWidget'))
        #     print(o.inherits('QPushButton'))
        # ********** API ********** 结束

        # ********** 案例 ********** 开始
        label1 = QLabel(self)
        label1.setText('11111')
        label2 = QLabel(self)
        label2.setText('22222')
        label2.move(100, 100)

        btn = QPushButton(self)
        btn.setText('点我')
        btn.move(200, 200)

        # for widget in self.findChildren(QLabel):
        for widget in self.children():
            # if widget.isWidgetType():
            if widget.inherits("QLabel"):
                print(widget)
                widget.setStyleSheet('background:cyan')

        # ********** 案例 ********** 结束

    # QObeject对象删除
    def QObjectDelObjTest(self):
        # ********** API ********** 开始
        obj1 = QObject()
        self.obj1 = obj1

        obj2 = QObject()
        obj3 = QObject()

        obj3.setParent(obj2)
        obj2.setParent(obj1)
        # ********** API ********** 结束

        obj1.destroyed.connect(lambda : print('obj1被释放了'))
        obj2.destroyed.connect(lambda : print('obj2被释放了'))
        obj3.destroyed.connect(lambda : print('obj3被释放了'))

        # 不会直接删除对象 只是删除引用
        # del obj2

        obj2.deleteLater()
        print(obj1.children())

        pass


if __name__ == '__main__':
    import sys
    app = QApplication(sys.argv)

    window = Window()
    window.show()

    # window = QWidget()

    # def cao(title):
    #     print('标题改变了', title)
    #     # window.windowTitleChanged.disconnect()
    #     # window.setWindowTitle('Test-' + title)
    #     # window.windowTitleChanged.connect(cao)
    #     window.blockSignals(True)
    #     window.setWindowTitle('Test-' + title)
    #     window.blockSignals(False)
    #
    # window.windowTitleChanged.connect(cao)

    # window.setWindowTitle('Hello SZ')
    # window.setWindowTitle('Hello SZ2')

    # window.show()


    sys.exit(app.exec_())