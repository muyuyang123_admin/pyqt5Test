#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Created on 2021/12/29 0029:12:37
@author: muyuyang123
@site: https://gitee.com/muyuyang123_admin
@email: 13838164635@163.com
@file: 
@description: 
"""
from PyQt5.Qt import *

class Window(QWidget):
    def __init__(self):
        super().__init__()
        self.setWindowTitle("QObeject的学习")
        self.resize(500, 500)
        self.setup_ui()

    def setup_ui(self):
        # self.QObejectInheritTest()
        self.QObejectAPITest()
        pass

    def QObejectInheritTest(self):
        mros = QObject.mro()
        for mro in mros:
            print(mro)


    def QObejectAPITest(self):
        # 测试API
        obj = QObject()
        obj.setObjectName('notice')
        print(obj.objectName())


if __name__ == '__main__':
    import sys
    app = QApplication(sys.argv)

    window = Window()

    window.show()

    sys.exit(app.exec_())