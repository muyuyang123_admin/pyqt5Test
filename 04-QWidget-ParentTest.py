#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Created on 2022/1/23
@author: muyuyang123
@site: https://gitee.com/muyuyang123_admin
@email: 13838164635@163.com
@file: 
@description: 
"""
# 0. 导入需要的包和模块
from PyQt5.Qt import *  # 主要包含了我们常用的一些类, 汇总到了一块
import sys
from PyQt5 import QtGui

class MyLabel(QLabel):
    def mousePressEvent(self, ev: QtGui.QMouseEvent) -> None:
        self.setStyleSheet('background: red;')

class MyWindow(QWidget):
    def mousePressEvent(self, a0: QtGui.QMouseEvent) -> None:
        print('mouse press at ', a0.x(), a0.y())
        myObj = self.childAt(a0.x(), a0.y())
        print(myObj)
        if myObj:
            myObj.setStyleSheet('background: red;')

        pass
# 1. 创建一个应用程序对象
app = QApplication(sys.argv)

# 2. 控件的操作
# 创建控件,设置控件(大小,位置,样式...),事件,信号的处理
# 2.1 创建控件
# window = QWidget()
window = MyWindow()

# 2.2 设置控件
window.setWindowTitle("父子关系案例")
window.resize(500, 500)

for i in range(1, 10):
    # label = MyLabel(window)
    label = QLabel(window)
    label.setText('label'+str(i))
    label.move(40*i, 40*i)

# 2.3 展示控件
window.show()

# 3. 应用程序的执行, 进入到消息循环
# 检测整个程序所接收到的用户的交互信息
sys.exit(app.exec_())
