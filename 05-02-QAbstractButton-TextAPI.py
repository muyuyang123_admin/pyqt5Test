# 0. 导入需要的包和模块
from PyQt5.Qt import *  # 主要包含了常用的一些类
import sys

# 1. 创建一个应用程序对象
# sys.argv是从用户处接收到的命令参数 这里传递给Qt的程序
app = QApplication(sys.argv)

# 2. 控件的操作
# 2.1 创建控件
window = QWidget()

# 2.2 设置控件
window.setWindowTitle("QAbstractButton提示文本")
window.resize(500, 500)
btn = QPushButton(window)

# *************** 提示文本 *************** 开始
btn.setText('0')


def plusOne():
    btn.setText(str(int(btn.text()) + 1))


# btn.clicked.connect(lambda: btn.setText(str(int(btn.text())+1)))

# *************** 提示文本 *************** 开始

# *************** 图标设置 *************** 开始
# icon = QIcon('xxx.png')
# btn.setIcon(icon)
# qsize = QSize(50, 50)
# btn.setIconSize(qsize)
# *************** 图标设置 *************** 开始

# *************** 快捷键设置 *************** 开始
# # btn.setText('&btn')
# btn.setShortcut('Alt+a')
# *************** 快捷键设置 *************** 开始

# *************** 自动重复 *************** 开始
# btn.setAutoRepeat(True)
# # 默认初始等待时间是300ms 间隔时间为100ms
# btn.setAutoRepeatDelay(1000)
# btn.setAutoRepeatInterval(500)
# # print(btn.autoRepeatDelay())
# # print(btn.autoRepeatInterval())
# btn.clicked.connect(lambda: btn.setText(str(int(btn.text())+1)))
# btn.clicked.connect(plusOne)
# *************** 自动重复 *************** 开始
# btn.clicked.connect(lambda: print('按钮被点击了'))

# *************** 状态设置 *************** 开始
# pushBtn = QPushButton(window)
# pushBtn.setText('pushBtn')
# pushBtn.move(100, 100)
# 
# radioBtn = QRadioButton(window)
# radioBtn.setText('radioBtn')
# radioBtn.move(100, 150)
# 
# checkBtn = QCheckBox(window)
# checkBtn.setText('checkBtn')
# checkBtn.move(100, 200)
# 
# pushBtn.setStyleSheet('QPushButton:pressed {background-color: red; }')

# 把三个按钮设置为按下状态
# pushBtn.setDown(True)
# radioBtn.setDown(True)
# checkBtn.setDown(True)

# pushBtn.setCheckable(True)
# print(pushBtn.isCheckable())
# print(radioBtn.isCheckable())
# print(checkBtn.isCheckable())
# 
# pushBtn.setChecked(True)
# radioBtn.setChecked(True)
# checkBtn.setChecked(True)
# print(pushBtn.isChecked())
# print(radioBtn.isChecked())
# print(checkBtn.isChecked())
# 
# def cao():
#     pushBtn.toggle()
#     radioBtn.toggle()
#     checkBtn.toggle()
# 
# btn.pressed.connect(cao)
# 
# pushBtn.setEnabled(False)
# radioBtn.setEnabled(False)
# checkBtn.setEnabled(False)
# *************** 状态设置 *************** 开始

# *************** 模拟点击 *************** 开始
# cBtn = QPushButton(window)
# cBtn.setText('模拟点击')
# cBtn.move(250, 250)
# cBtn.clicked.connect(lambda : print('点击按钮了'))
# # cBtn.click()
# # cBtn.animateClick(2000)
# 
# cBtn2 = QPushButton(window)
# cBtn2.setText('模拟点击2')
# cBtn2.move(250, 350)
# def cCao():
#     cBtn.animateClick(2000)
# 
# cBtn2.clicked.connect(cCao)
# *************** 模拟点击 *************** 开始

# 2.3 展示控件

# *************** 设置有效区域 *************** 开始


# *************** 设置有效区域 *************** 开始


# *************** 可用信号 *************** 开始
btn.setCheckable(True)
btn.pressed.connect(lambda : print('按钮被按下'))
btn.released.connect(lambda : print('按钮被松开'))
btn.clicked.connect(lambda value : print('按钮被点击了 ', value))
btn.toggled.connect(lambda value: print('按钮选中状态发生了改变', value))
# *************** 可用信号 *************** 开始
window.show()

# 3. 应用程序的执行，进入到消息循环
# 让整个程序开始执行， 并且进入到消息循环（无线循环）
# 检测整个程序所接收到的用户的交互信息

sys.exit(app.exec_())
