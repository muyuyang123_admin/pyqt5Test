#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Created on 2022/1/9
@author: muyuyang123
@site: https://gitee.com/muyuyang123_admin
@email: 13838164635@163.com
@file: 
@description: 
"""

# 0. 导入需要的包和模块
from PyQt5.Qt import *  # 主要包含了我们常用的一些类, 汇总到了一块
import sys

class MyObj(QObject):
    def timerEvent(self, evt):
        print(evt, '1')

        pass

class MyQLabel(QLabel):

    def __init__(self, *args, **kwargs):
        super(MyQLabel, self).__init__(*args, **kwargs)
        self.setText('10')
        self.move(100, 100)
        self.setStyleSheet('font-size:40px')
        # self.timerID = self.startTimer(1000)


    # 设置倒计时起点
    def setSec(self, sec):
        self.setText(str(sec))

    def startMyTimer(self, ms):
        # self.setSec(ms)
        self.timerID = self.startTimer(ms)

    def timerEvent(self, evt):
        currentSec = int(self.text())
        currentSec -= 1
        if currentSec == 0:
            print('stop')
            self.killTimer(self.timerID)
        self.setText(str(currentSec))

        pass

class MyWidget(QWidget):
    def timerEvent(self, *args, **kwargs):
        currentW = self.width()
        currentH = self.height()
        currentW += 10
        currentH += 10
        self.resize(currentW, currentH)

# 1. 创建一个应用程序对象
app = QApplication(sys.argv)

# 2. 控件的操作
# 创建控件,设置控件(大小,位置,样式...),事件,信号的处理
# 2.1 创建控件
window = QWidget()
# window = MyWidget()

# 2.2 设置控件
window.setWindowTitle("QObject定时器使用")
window.resize(500, 500)

# obj = QObject()
# obj = MyObj()

# label = QLabel(window)
# label.setText('10')
# label.move(100, 100)
# label.setStyleSheet('font-size:40px')
# timerID = label.startTimer(1000)
label = MyQLabel(window)
label.move(100, 100)
label.setStyleSheet('font-size:40px')
label.setSec(5)
label.startMyTimer(500)

# 2.3 展示控件
window.show()

# 3. 应用程序的执行, 进入到消息循环
# 检测整个程序所接收到的用户的交互信息
sys.exit(app.exec_())
