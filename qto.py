#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Created on 2021/12/28 0028:12:56
@author: muyuyang123
@site: https://gitee.com/muyuyang123_admin
@email: 13838164635@163.com
@file: 
@description: 
"""

from PyQt5.Qt import *

class Window(QWidget):
    def __init__(self):
        super().__init__()
        self.setWindowTitle("的学习")
        self.resize(500, 500)
        self.setup_ui()

    def setup_ui(self):
        
        pass



if __name__ == '__main__':
    import sys
    app = QApplication(sys.argv)

    window = Window()

    window.show()

    sys.exit(app.exec_())