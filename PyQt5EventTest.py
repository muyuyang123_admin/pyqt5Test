#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Created on 2022/1/6 0006:12:33
@author: muyuyang123
@site: https://gitee.com/muyuyang123_admin
@email: 13838164635@163.com
@file: 
@description: 
"""

from PyQt5.Qt import *  
import sys

class AppTest(QApplication):

    # def notify(self, a0: QtCore.QObject, a1: QtCore.QEvent) -> bool:
    # 重写方法
    def notify(self, recevier, evt):
        # 筛选事件接收者按钮和事件类型
        if recevier.inherits('QPushButton') and evt.type() == QEvent.MouseButtonPress:
            # print(recevier, evt)
            pass
        # 调用父类方法
        return super().notify(recevier, evt)


    pass

class Btn(QPushButton):
    def event(self, evt):
        if evt.type() == QEvent.MouseButtonPress:
            print('按钮被点击了-T')
            print(evt)
        return super().event(evt)

    def mousePressEvent(self, *args, **kwargs):
        print('鼠标被按下了')
        return super().mousePressEvent(*args, **kwargs)

# 1. 创建一个应用程序对象
app = AppTest(sys.argv)

# 2. 控件的操作
# 2.1 创建控件
window = QWidget()

btn = Btn(window)
# btn = QPushButton(window)
btn.setText('按钮')
btn.move(100, 100)

def cao():
    print('按钮被点击了')

btn.pressed.connect(cao)

# 2.2 设置控件
window.setWindowTitle("QObeject事件的学习")
window.resize(500, 500)


# 2.3 展示控件
window.show()

# 3. 应用程序的执行, 进入到消息循环
sys.exit(app.exec_())
