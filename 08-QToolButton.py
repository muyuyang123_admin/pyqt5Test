
# 0. 导入需要的包和模块
from PyQt5.Qt import *  # 主要包含了常用的一些类
import sys


# 1. 创建一个应用程序对象
# sys.argv是从用户处接收到的命令参数 这里传递给Qt的程序
app = QApplication(sys.argv)

# 2. 控件的操作
# 2.1 创建控件
window = QWidget()


# 2.2 设置控件
window.setWindowTitle("QToolButton学习")
window.resize(500, 500)

tb = QToolButton(window)
# tb.setArrowType(Qt.RightArrow)

# tb = QPushButton(window)
tb.setText('Tools')
tb.setIcon(QIcon('xxx.png'))
tb.setToolTip('这是一个新建按钮')
# tb.setToolTipDuration(1000)

# *************** 工具按钮图标样式 *************** 开始
# 仅显示图标 Qt.ToolButtonIconOnly
# tb.setToolButtonStyle(Qt.ToolButtonIconOnly)
# 仅显示文本
# tb.setToolButtonStyle(Qt.ToolButtonTextOnly)
# 文字在图标下部
# tb.setToolButtonStyle(Qt.ToolButtonTextUnderIcon)
# 文字在图标旁边
tb.setToolButtonStyle(Qt.ToolButtonTextBesideIcon)
# Qt.ToolButtonFollowStyle
# 	遵循风格
# tb.setToolButtonStyle(Qt.ToolButtonFollowStyle)
# *************** 工具按钮图标样式 *************** 结束

# *************** 箭头样式 *************** 开始
# Qt.NoArrow
# 	无箭头
# Qt.UpArrow
# 	向上箭头
# Qt.DownArrow
# 	向下箭头
# Qt.LeftArrow
# 	向左箭头
# Qt.RightArrow
# 	向右箭头
# 箭头样式优先级高于图标和文本
tb.setArrowType(Qt.RightArrow)
# *************** 箭头样式 *************** 结束

# *************** 自动提升 *************** 开始
tb.setAutoRaise(True)
# *************** 自动提升 *************** 结束

# *************** 菜单设置 *************** 开始
btn = QPushButton('一般按钮', window)
btn.move(150, 150)
btn.setFlat(True)

menu = QMenu(btn)
menu.setTitle('菜单')
# 创建菜单对象
subMenu = QMenu('子菜单', menu)
subMenu.setIcon(QIcon('xxx.png'))
action1 = QAction(QIcon('xxx.png'), '行为', menu)
action1.triggered.connect(lambda : print('行为1'))
# 将菜单对象加入到菜单中
menu.addMenu(subMenu)
menu.addSeparator()
menu.addAction(action1)

btn.setMenu(menu)


menuTb = QMenu(tb)
menuTb.setTitle('菜单')
# 创建菜单对象
subMenu2 = QMenu('子菜单', menuTb)
subMenu2.setIcon(QIcon('xxx.png'))
action2 = QAction(QIcon('xxx.png'), '行为', menuTb)
action2.triggered.connect(lambda : print('行为2'))
# 将菜单对象加入到菜单中
menuTb.addMenu(subMenu2)
menuTb.addSeparator()
menuTb.addAction(action2)

tb.setMenu(menuTb)

# setPopupMode(QToolButton.ToolButtonPopupMode)
# 	QToolButton.ToolButtonPopupMode
# 		QToolButton.DelayedPopup
# 			鼠标按住一会才显示
# 			类似于浏览器后退按钮
# 		QToolButton.MenuButtonPopup
# 			有一个专门的指示箭头
# 			点击箭头才显示
# 		QToolButton.InstantPopup
# 			点了按钮就显示
# 			点击信号不会发射
tb.setPopupMode(QToolButton.DelayedPopup)

tb.clicked.connect(lambda : print('工具按钮被点击了'))

# *************** 菜单设置 *************** 结束


# 2.3 展示控件
window.show()

# 3. 应用程序的执行，进入到消息循环
# 让整个程序开始执行， 并且进入到消息循环（无线循环）
# 检测整个程序所接收到的用户的交互信息

sys.exit(app.exec_())