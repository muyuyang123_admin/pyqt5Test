#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Created on 2022/1/22
@author: muyuyang123
@site: https://gitee.com/muyuyang123_admin
@email: 13838164635@163.com
@file: 
@description: 
"""

# 0. 导入需要的包和模块
from PyQt5.Qt import *
from PyQt5 import QtCore


class Window(QWidget):
    def __init__(self):
        super(Window, self).__init__()
        self.setWindowTitle("鼠标事件案例1")
        self.resize(500, 500)
        self.setup_ui()

    def setup_ui(self):
        pass


class MyLabel(QLabel):
    def enterEvent(self, a0: QtCore.QEvent) -> None:
        print('欢迎光临')
        self.setText('欢迎光临')

    def leaveEvent(self, a0: QtCore.QEvent) -> None:
        print('谢谢惠顾')
        self.setText('谢谢惠顾')

if __name__ == '__main__':
    import sys

    # 1. 创建一个应用程序对象
    app = QApplication(sys.argv)

    # 2. 控件的操作
    # 创建控件,设置控件(大小,位置,样式...),事件,信号的处理
    # 2.1 创建控件
    window = Window()

    # 2.2 设置控件
    # window.setWindowTitle("pyqt学习")
    # window.resize(500, 500)
    label = MyLabel(window)
    label.setText('Label')
    label.resize(200, 100)
    label.move(100, 100)
    label.setStyleSheet('background: red;')

    # 2.3 展示控件
    window.show()

    # 3. 应用程序的执行, 进入到消息循环
    # 检测整个程序所接收到的用户的交互信息
    sys.exit(app.exec_())