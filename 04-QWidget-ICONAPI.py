
# 0. 导入需要的包和模块
from PyQt5.Qt import *  # 主要包含了常用的一些类
import sys
from PyQt5 import QtGui


class MyWindow(QWidget):
    def mousePressEvent(self, a0: QtGui.QMouseEvent) -> None:
        if self.isMaximized():
            self.showNormal()
        else:
            self.showMaximized()
# 1. 创建一个应用程序对象
# sys.argv是从用户处接收到的命令参数 这里传递给Qt的程序
app = QApplication(sys.argv)

# 2. 控件的操作
# 2.1 创建控件
# window = QWidget()
window = MyWindow()


# 2.2 设置控件
# window.setWindowTitle("QWidget图标")
# window.resize(500, 500)
# print(window.windowTitle())
#
# icon=QIcon('xxx.png')
# print(icon)
# window.setWindowIcon(icon)
# print(window.windowIcon())
#
# window.setWindowOpacity(0.9)
# print(window.windowOpacity())
#
# print(window.windowState()==Qt.WindowNoState)


# window.setWindowState(Qt.WindowMinimized)
# window.setWindowState(Qt.WindowMaximized)
# window.setWindowState(Qt.WindowFullScreen)

# w2 = QWidget()
# w2.setWindowTitle('w2')


print(window.windowState())


# 2.3 展示控件
window.show()
# w2.show()
# window.setWindowState(Qt.WindowActive)

# window.showMaximized()
# window.showMinimized()
# window.showFullScreen()
# window.showNormal()

# 3. 应用程序的执行，进入到消息循环
# 让整个程序开始执行， 并且进入到消息循环（无线循环）
# 检测整个程序所接收到的用户的交互信息

sys.exit(app.exec_())