#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Created on 2022/1/4 0004:12:46
@author: muyuyang123
@site: https://gitee.com/muyuyang123_admin
@email: 13838164635@163.com
@file: 
@description: 
"""
from PyQt5.Qt import *

class Window(QWidget):
    def __init__(self):
        super().__init__()
        self.setWindowTitle("QObeject的学习")
        self.resize(500, 500)
        self.setup_ui()

    def setup_ui(self):
        # self.QObejectInheritTest()
        # self.QObejectAPITest()
        self.QObejectSignalTest()
        pass

    def QObejectInheritTest(self):
        mros = QObject.mro()
        for mro in mros:
            print(mro)


    def QObejectAPITest(self):
        # 测试API
        obj = QObject()
        obj.setObjectName('notice')
        print(obj.objectName())


    def QObejectSignalTest(self):
        # ********** 信号与槽测试案例 **********
        #
        # btn = QPushButton(self)
        # btn.setText('点击我')
        #
        # def cao():
        #     print('点击我嘎哈')
        #
        # btn.clicked.connect(cao)
        # ********** 信号与槽测试案例 **********
        pass



if __name__ == '__main__':
    import sys
    app = QApplication(sys.argv)

    # window = Window()
    # window.show()

    window = QWidget()

    def cao(title):
        print('标题改变了', title)
        # window.windowTitleChanged.disconnect()
        # window.setWindowTitle('Test-' + title)
        # window.windowTitleChanged.connect(cao)
        window.blockSignals(True)
        window.setWindowTitle('Test-' + title)
        window.blockSignals(False)

    window.windowTitleChanged.connect(cao)

    window.setWindowTitle('Hello SZ')
    # window.setWindowTitle('Hello SZ2')

    window.show()


    sys.exit(app.exec_())