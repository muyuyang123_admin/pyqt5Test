
# 0. 导入需要的包和模块
from PyQt5.Qt import *  # 主要包含了常用的一些类
import sys
from PyQt5 import QtGui

class Window(QWidget):
    def paintEvent(self, a0: QtGui.QPaintEvent) -> None:
        print('窗口被绘制了')
        return super().paintEvent(a0)

class Btn(QPushButton):
    def paintEvent(self, a0: QtGui.QPaintEvent) -> None:
        print('按钮被绘制了')
        return super().paintEvent(a0)
# 1. 创建一个应用程序对象
# sys.argv是从用户处接收到的命令参数 这里传递给Qt的程序
app = QApplication(sys.argv)

# 2. 控件的操作
# 2.1 创建控件
# window = QWidget()
window = Window()


# 2.2 设置控件
window.setWindowTitle("交互状态学习")
window.resize(500, 500)

# btn = QPushButton(window)
btn = Btn(window)
btn.setText('btn')

# btn1 = QPushButton(window)
# btn1.move(100, 100)
# btn1.setText('btn1')
# btn2 = QPushButton(window)
# btn2.move(100, 200)
# btn2.setText('btn2')
#
# def btnEnable():
#     # if btn2.isEnabled():
#     #     btn2.setDisabled(True)
#     # else:
#     #     btn2.setEnabled(True)
#     if btn2.isVisible():
#         btn2.setVisible(False)
#     else:
#         btn2.setVisible(True)
# btn1.clicked.connect(btnEnable)



# 2.3 展示控件
window.show()
btn.pressed.connect(lambda: btn.setVisible(True))

# 3. 应用程序的执行，进入到消息循环
# 让整个程序开始执行， 并且进入到消息循环（无线循环）
# 检测整个程序所接收到的用户的交互信息

sys.exit(app.exec_())