
# 0. 导入需要的包和模块
from PyQt5.Qt import *  # 主要包含了常用的一些类
import sys
from PyQt5 import QtGui

class Window(QWidget):
    def mousePressEvent(self, a0: QtGui.QMouseEvent) -> None:
        # print(self.focusWidget())
        # self.focusNextChild()
        # self.focusPreviousChild()
        self.focusNextPrevChild(True)
# 1. 创建一个应用程序对象
# sys.argv是从用户处接收到的命令参数 这里传递给Qt的程序
app = QApplication(sys.argv)

# 2. 控件的操作
# 2.1 创建控件
# window = QWidget()
window = Window()


# 2.2 设置控件
window.setWindowTitle("焦点控制")
window.resize(500, 500)


le = QLineEdit(window)
le.setText('le')
le.move(100, 50)

le1 = QLineEdit(window)
le1.setText('le1')
le1.move(100, 150)

le2 = QLineEdit(window)
le2.setText('le2')
le2.move(100, 250)

# le1.setFocus()
#
# le.clearFocus()

# le1.setFocusPolicy(Qt.TabFocus)
# le1.setFocusPolicy(Qt.ClickFocus)
# le1.setFocusPolicy(Qt.StrongFocus)

print(le)
print(le1)
print(le2)
le1.setFocus()
# 2.3 展示控件
window.show()

# 获取当前控件中获取焦点的那个
print(window.focusWidget())

window.setTabOrder(le, le2)
window.setTabOrder(le2, le1)

# 3. 应用程序的执行，进入到消息循环
# 让整个程序开始执行， 并且进入到消息循环（无线循环）
# 检测整个程序所接收到的用户的交互信息

sys.exit(app.exec_())